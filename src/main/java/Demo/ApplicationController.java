package Demo;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ApplicationController {

    @RequestMapping("/application")
    public String index() {
        return "Book Shop";
    }

}


